package com.bezkoder.springjwt.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.bezkoder.springjwt.models.Customer;
import com.bezkoder.springjwt.models.SaleBill;
import com.bezkoder.springjwt.repository.CustomerRepository;
import com.bezkoder.springjwt.repository.ItemRepository;
import com.bezkoder.springjwt.repository.SaleBillRepository;
import com.bezkoder.springjwt.repository.UserRepository;
import com.bezkoder.springjwt.security.jwt.AuthEntryPointJwt;
import com.bezkoder.springjwt.security.jwt.JwtUtils;
import com.bezkoder.springjwt.security.services.UserDetailsServiceImpl;

@WebMvcTest(SaleBillController.class)
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class SaleBillControllerTest {
	@MockBean
	private UserDetailsServiceImpl userDetailsService;

	@MockBean
	private AuthEntryPointJwt unauthorizedHandler;
	
	@MockBean
	private JwtUtils jwtUtils;
	
	@MockBean
	private UserRepository userRepository;

	@MockBean
	private SaleBillRepository saleBillRepository;
	
	@MockBean
	private ItemRepository itemRepository;
	
	@MockBean
	private CustomerRepository customerRepository;
	
	@Autowired
	private MockMvc mockMvc;
	
	// lấy về tất cả hóa đơn
	@Test
	@WithMockUser(username="tester", roles={"ADMIN"})
	public void getSaleBill_test_1() throws Exception {
		SaleBill saleBill1 = new SaleBill();
		saleBill1.setDate(new Date());
		SaleBill saleBill2 = new SaleBill();
		saleBill2.setDate(new Date());
		
		List<SaleBill> saleBills = Arrays.asList(saleBill1, saleBill2);
		doReturn(saleBills).when(saleBillRepository).findAll();
		
		mockMvc.perform(get("/api/v1/sale_bill"))
        		
        		.andExpect(status().is2xxSuccessful())
        		.andExpect(jsonPath("$", Matchers.hasSize(2)));
	}
	
	// lấy về tất cả hóa đơn theo khách hàng
	@Test
	@WithMockUser(username="tester", roles={"ADMIN"})
	public void getAllByCustomerId_test_1() throws Exception {
		Customer customer = new Customer();
		customer.setId(1);

		SaleBill saleBill1 = new SaleBill();
		saleBill1.setDate(new Date());
		saleBill1.setCustomer(customer);

		SaleBill saleBill2 = new SaleBill();
		saleBill2.setDate(new Date());
		saleBill2.setCustomer(customer);
		
		List<SaleBill> saleBills = Arrays.asList(saleBill1, saleBill2);
		doReturn(saleBills).when(saleBillRepository).findByCustomer(customer);
		
		mockMvc.perform(get("/api/v1/sale_bill/1"))
        		
        		.andExpect(status().is2xxSuccessful())
        		.andExpect(jsonPath("$", Matchers.hasSize(2)));
	}
}
