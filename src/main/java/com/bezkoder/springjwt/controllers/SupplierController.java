package com.bezkoder.springjwt.controllers;

import com.bezkoder.springjwt.exception.ResourceNotFoundException;
import com.bezkoder.springjwt.models.Supplier;
import com.bezkoder.springjwt.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class SupplierController {
    @Autowired
    private SupplierRepository supplierRepository;


    @GetMapping("/suppliers")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public List<Supplier> getSupplier() {
        return supplierRepository.findAll();
    }

    @GetMapping("/suppliers_statistics")
    @PreAuthorize(" hasRole('USER')  or hasRole('ADMIN')")
    public List<Supplier> getSupplierStatistics() {
        List<Supplier> result = supplierRepository.findAll(Sort.by(Sort.Direction.DESC, "money"));
        return  result;
    }

    @PostMapping("/suppliers")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public Supplier createSupplier(@RequestBody Supplier supplier) {
        return supplierRepository.save(supplier);
    }

    @PutMapping("/suppliers/{supplierId}")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public Supplier updateSupplier(@PathVariable int supplierId, @RequestBody Supplier supplierRequest) {
        return supplierRepository.findById(supplierId).map(supplierNew -> {
            supplierNew.setName(supplierRequest.getName());
            supplierNew.setPhone(supplierRequest.getPhone());

            return supplierRepository.save(supplierNew);
        }).orElseThrow(() -> new ResourceNotFoundException("SupplierId " + supplierId + " not found"));
    }


    @DeleteMapping("/suppliers/{supplierId}")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<?> deleteSupplier(@PathVariable int supplierId) {
        return supplierRepository.findById(supplierId).map(supplierDelete -> {
            supplierRepository.delete(supplierDelete);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("SupplierId " + supplierId + " not found"));
    }
}
