package com.bezkoder.springjwt.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.bezkoder.springjwt.exception.ResourceNotFoundException;
import com.bezkoder.springjwt.models.Customer;
import com.bezkoder.springjwt.repository.CustomerRepository;
import com.bezkoder.springjwt.repository.UserRepository;
import com.bezkoder.springjwt.security.jwt.AuthEntryPointJwt;
import com.bezkoder.springjwt.security.jwt.JwtUtils;
import com.bezkoder.springjwt.security.services.UserDetailsServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

@WebMvcTest(CustomerController.class)
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class CustomerControllerTest {
	@MockBean
	private UserDetailsServiceImpl userDetailsService;

	@MockBean
	private AuthEntryPointJwt unauthorizedHandler;
	
	@MockBean
	private JwtUtils jwtUtils;
	
	@MockBean
	private UserRepository userRepository;
	
	@MockBean
	private CustomerRepository customerRepository;

	@Autowired
	private MockMvc mockMvc;

	// lấy về tất cả khách hàng
	@Test
	@WithMockUser(username = "tester", roles = { "ADMIN" })
	public void getCustomers_test_1() throws Exception {
		Customer customer1 = new Customer();
		customer1.setName("Nguyen Van A");
		Customer customer2 = new Customer();
		customer2.setName("Tran Thi B");

		List<Customer> customers = Arrays.asList(customer1, customer2);
		doReturn(customers).when(customerRepository).findAll();

		mockMvc.perform(get("/api/v1/customers"))

				.andExpect(status().is2xxSuccessful()).andExpect(jsonPath("$", Matchers.hasSize(2)))
				.andExpect(jsonPath("$[0].name", is("Nguyen Van A")));
	}

	// lấy về thống kê khách hàng
	@Test
	@WithMockUser(username = "tester", roles = { "ADMIN" })
	public void getCustomerStatistics_test_1() throws Exception {
		Customer customer1 = new Customer();
		customer1.setName("Nguyen Van A");
		customer1.setMoney(20000);
		Customer customer2 = new Customer();
		customer2.setName("Tran Thi B");
		customer2.setMoney(1000);

		List<Customer> customers = Arrays.asList(customer1, customer2);
		doReturn(customers).when(customerRepository).findAll();

		mockMvc.perform(get("/api/v1/customers"))

				.andExpect(status().is2xxSuccessful())
				.andExpect(jsonPath("$", Matchers.hasSize(2)))
				.andExpect(jsonPath("$[0].name", is("Nguyen Van A")))
				.andExpect(jsonPath("$[0].money", is(20000)));
	}

	// tạo mới khách hàng
	@Test
	@WithMockUser(username = "tester", roles = { "ADMIN" })
	public void createCustomer_test_1() throws Exception {
		Customer customer = new Customer();
		customer.setDob(new Date());
		customer.setMoney(2000);
		customer.setName("Le Van C");
		customer.setPhoneNumber("0354544233");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(customer);
		
		mockMvc.perform(post("/api/v1/customers")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))

				.andExpect(status().is2xxSuccessful());
	}

	// xóa khách hàng thành công
	@Test
	@WithMockUser(username = "tester", roles = { "ADMIN" })
	public void deleteCustomer_test_1() throws Exception {
		Customer customer = new Customer();
		customer.setId(1);
		customer.setDob(new Date());
		customer.setMoney(2000);
		customer.setName("Le Van C");
		customer.setPhoneNumber("0354544233");

		doReturn(Optional.of(customer)).when(customerRepository).findById(customer.getId());

		mockMvc.perform(delete("/api/v1/customers/1"))

				.andExpect(status().is2xxSuccessful());
	}

	// xóa khách hàng thất bại, không tìm thấy khách hàng
	@Test
	@WithMockUser(username = "tester", roles = { "ADMIN" })
	public void deleteCustomer_test_2() throws Exception {
		doReturn(Optional.empty()).when(customerRepository).findById(1);

		mockMvc.perform(delete("/api/v1/customers/1"))

				.andExpect(status().isNotFound())
				.andExpect(result -> assertTrue(result.getResolvedException() instanceof ResourceNotFoundException))
				.andExpect(
						result -> assertEquals("CustomerId 1 not found", result.getResolvedException().getMessage()));
	}
}
