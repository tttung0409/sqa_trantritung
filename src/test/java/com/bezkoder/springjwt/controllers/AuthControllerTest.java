package com.bezkoder.springjwt.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.bezkoder.springjwt.models.ERole;
import com.bezkoder.springjwt.models.Role;
import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.payload.request.LoginRequest;
import com.bezkoder.springjwt.payload.request.SignupRequest;
import com.bezkoder.springjwt.repository.RoleRepository;
import com.bezkoder.springjwt.repository.UserRepository;
import com.bezkoder.springjwt.security.jwt.AuthEntryPointJwt;
import com.bezkoder.springjwt.security.jwt.JwtUtils;
import com.bezkoder.springjwt.security.services.UserDetailsServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

@WebMvcTest(AuthController.class)
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class AuthControllerTest {
	@MockBean
	private UserDetailsServiceImpl userDetailsService;

	@MockBean
	private AuthEntryPointJwt unauthorizedHandler;
	
	@MockBean
	AuthenticationManager authenticationManager;

	@MockBean
	UserRepository userRepository;

	@MockBean
	RoleRepository roleRepository;

	@MockBean
	PasswordEncoder encoder;

	@MockBean
	JwtUtils jwtUtils;
	
	@Autowired
	private MockMvc mockMvc;
	
	// đăng ký thành công
	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN" })
	public void registerUser_test_1() throws Exception {
		HashSet<Role> roles = new HashSet<>();
		Role roleAdmin = new Role();
		roleAdmin.setId(1);
		roleAdmin.setName(ERole.ROLE_ADMIN);
		roles.add(roleAdmin);
		
		Role roleUser = new Role();
		roleUser.setId(2);
		roleUser.setName(ERole.ROLE_USER);

		doReturn(Optional.of(roleAdmin)).when(roleRepository).findByName(ERole.ROLE_ADMIN);
		doReturn(Optional.of(roleUser)).when(roleRepository).findByName(ERole.ROLE_USER);
		doReturn(false).when(userRepository).existsByUsername("test");
		doReturn(false).when(userRepository).existsByEmail("test@gmail.com");

		SignupRequest signupRequest = new SignupRequest();
		signupRequest.setUsername("test");
		signupRequest.setPassword("abcd1234");
		signupRequest.setRole(Set.of("admin"));
		signupRequest.setEmail("test@gmail.com");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(signupRequest);
		
		mockMvc.perform(post("/api/v1/sign_up")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))

				.andExpect(status().is2xxSuccessful())
				.andExpect(jsonPath("$.message", is("User registered successfully!")));
	}
	
	// đăng ký thành công, để trống roles
	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN" })
	public void registerUser_test_2() throws Exception {
		HashSet<Role> roles = new HashSet<>();
		Role roleAdmin = new Role();
		roleAdmin.setId(1);
		roleAdmin.setName(ERole.ROLE_ADMIN);
		roles.add(roleAdmin);
		
		Role roleUser = new Role();
		roleUser.setId(2);
		roleUser.setName(ERole.ROLE_USER);

		doReturn(Optional.of(roleAdmin)).when(roleRepository).findByName(ERole.ROLE_ADMIN);
		doReturn(Optional.of(roleUser)).when(roleRepository).findByName(ERole.ROLE_USER);
		doReturn(false).when(userRepository).existsByUsername("test");
		doReturn(false).when(userRepository).existsByEmail("test@gmail.com");

		SignupRequest signupRequest = new SignupRequest();
		signupRequest.setUsername("test");
		signupRequest.setPassword("abcd1234");
		signupRequest.setEmail("test@gmail.com");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(signupRequest);
		
		mockMvc.perform(post("/api/v1/sign_up")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))

				.andExpect(status().is2xxSuccessful())
				.andExpect(jsonPath("$.message", is("User registered successfully!")));
	}
	
	// đăng ký thất bại, để trống các trường
	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN" })
	public void registerUser_test_3() throws Exception {
		SignupRequest signupRequest = new SignupRequest();

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(signupRequest);
		
		mockMvc.perform(post("/api/v1/sign_up")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))

				.andExpect(status().is4xxClientError());
	}
	
	// đăng ký thất bại, username < 3 ký tự
	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN" })
	public void registerUser_test_4() throws Exception {
		SignupRequest signupRequest = new SignupRequest();
		signupRequest.setUsername("a");
		signupRequest.setPassword("abcd1234");
		signupRequest.setRole(Set.of("admin"));
		signupRequest.setEmail("test@gmail.com");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(signupRequest);
		
		mockMvc.perform(post("/api/v1/sign_up")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))

				.andExpect(status().is4xxClientError());
	}
	
	// đăng ký thất bại, username > 20 ký tự
	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN" })
	public void registerUser_test_5() throws Exception {
		SignupRequest signupRequest = new SignupRequest();
		signupRequest.setUsername("Day la mot cai ten vo cung dai dong");
		signupRequest.setPassword("abcd1234");
		signupRequest.setRole(Set.of("admin"));
		signupRequest.setEmail("test@gmail.com");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(signupRequest);
		
		mockMvc.perform(post("/api/v1/sign_up")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))

				.andExpect(status().is4xxClientError());
	}
	
	// đăng ký thất bại, email > 50 ký tự
	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN" })
	public void registerUser_test_6() throws Exception {
		SignupRequest signupRequest = new SignupRequest();
		signupRequest.setUsername("test");
		signupRequest.setPassword("abcd1234");
		signupRequest.setRole(Set.of("admin"));
		signupRequest.setEmail("daylaemailvocungdaidaylaemailvocungdaidaylaemailvocungdaidaylaemailvocungdaidaylaemailvocungdai@gmail.com");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(signupRequest);
		
		mockMvc.perform(post("/api/v1/sign_up")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))

				.andExpect(status().is4xxClientError());
	}
	
	// đăng ký thất bại, email sai định dạng
	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN" })
	public void registerUser_test_7() throws Exception {
		SignupRequest signupRequest = new SignupRequest();
		signupRequest.setUsername("test");
		signupRequest.setPassword("abcd1234");
		signupRequest.setRole(Set.of("admin"));
		signupRequest.setEmail("Day khong phai la email");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(signupRequest);
		
		mockMvc.perform(post("/api/v1/sign_up")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))

				.andExpect(status().is4xxClientError());
	}
	
	// đăng ký thất bại, mật khẩu < 6 ký tự
	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN" })
	public void registerUser_test_8() throws Exception {
		SignupRequest signupRequest = new SignupRequest();
		signupRequest.setUsername("test");
		signupRequest.setPassword("123");
		signupRequest.setRole(Set.of("admin"));
		signupRequest.setEmail("test@gmail.com");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(signupRequest);
		
		mockMvc.perform(post("/api/v1/sign_up")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))

				.andExpect(status().is4xxClientError());
	}
	
	// đăng ký thất bại, mật khẩu > 40 ký tự
	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN" })
	public void registerUser_test_9() throws Exception {
		SignupRequest signupRequest = new SignupRequest();
		signupRequest.setUsername("test");
		signupRequest.setPassword("111111111111111111111111111111111111111111111111111111111111111111");
		signupRequest.setRole(Set.of("admin"));
		signupRequest.setEmail("test@gmail.com");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(signupRequest);
		
		mockMvc.perform(post("/api/v1/sign_up")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))

				.andExpect(status().is4xxClientError());
	}
	
	// đăng ký thất bại, trùng username
	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN" })
	public void registerUser_test_10() throws Exception {
		doReturn(true).when(userRepository).existsByUsername("test");
		
		SignupRequest signupRequest = new SignupRequest();
		signupRequest.setUsername("test");
		signupRequest.setPassword("abcd1234");
		signupRequest.setRole(Set.of("admin"));
		signupRequest.setEmail("test@gmail.com");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(signupRequest);
		
		mockMvc.perform(post("/api/v1/sign_up")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))

				.andExpect(status().is4xxClientError())
				.andExpect(jsonPath("$.message", is("Error: Username is already taken!")));
	}

	// đăng ký thất bại, trùng email
	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN" })
	public void registerUser_test_11() throws Exception {
		doReturn(false).when(userRepository).existsByUsername("test");
		doReturn(true).when(userRepository).existsByEmail("test@gmail.com");

		
		SignupRequest signupRequest = new SignupRequest();
		signupRequest.setUsername("test");
		signupRequest.setPassword("abcd1234");
		signupRequest.setRole(Set.of("admin"));
		signupRequest.setEmail("test@gmail.com");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(signupRequest);
		
		mockMvc.perform(post("/api/v1/sign_up")
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestJson))

				.andExpect(status().is4xxClientError())
				.andExpect(jsonPath("$.message", is("Error: Email is already in use!")));
	}

}
