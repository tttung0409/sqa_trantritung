package com.bezkoder.springjwt.controllers;

import com.bezkoder.springjwt.exception.ResourceNotFoundException;
import com.bezkoder.springjwt.models.Customer;
import com.bezkoder.springjwt.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class CustomerController {
    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping("/customers")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public List<Customer> getCustomer() {
        return customerRepository.findAll();
    }

    //    @PreAuthorize("hasRole('ROLE_MEMBER')")
    @GetMapping("/customers_statistics")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public List<Customer> getCustomerStatistics() {
        List<Customer> result = customerRepository.findAll(Sort.by(Sort.Direction.DESC, "money"));
        return  result;
    }

    @PostMapping("/customers")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public Customer createCustomer(@RequestBody Customer customer) {
        return customerRepository.save(customer);
    }

    @DeleteMapping("/customers/{customerId}")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<?> deleteCustomer(@PathVariable int customerId) {
        return customerRepository.findById(customerId).map(customerDelete -> {
            customerRepository.delete(customerDelete);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("CustomerId " + customerId + " not found"));
    }
}
